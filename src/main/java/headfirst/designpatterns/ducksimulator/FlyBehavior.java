package headfirst.designpatterns.ducksimulator;

public interface FlyBehavior {
	public void fly();
}
