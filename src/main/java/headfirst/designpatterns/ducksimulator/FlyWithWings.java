package headfirst.designpatterns.ducksimulator;

public class FlyWithWings implements FlyBehavior {

	public void fly() {
		System.out.println("I'm flying !");
	}

}
