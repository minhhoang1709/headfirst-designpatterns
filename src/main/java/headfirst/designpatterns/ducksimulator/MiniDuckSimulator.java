package headfirst.designpatterns.ducksimulator;

public class MiniDuckSimulator {
	public static void main(String[] agrs) {
		Duck mallard = new MallardDuck();
		mallard.performFly();
		mallard.performQuack();
		
		Duck model = new ModelDuck();
		model.performFly();
		model.setFlyBehavior(new FlyRocketPowered());
		model.performFly();
		model.performQuack();
	}
}
