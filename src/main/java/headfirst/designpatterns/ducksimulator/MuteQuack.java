package headfirst.designpatterns.ducksimulator;

public class MuteQuack implements QuackBehavior {
	public void quack() {
		System.out.println("<<Silence>>");
	}
}
