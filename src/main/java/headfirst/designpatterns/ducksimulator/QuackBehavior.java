package headfirst.designpatterns.ducksimulator;

public interface QuackBehavior {
	public void quack();
}
