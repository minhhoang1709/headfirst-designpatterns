package headfirst.designpatterns.starbuzz;

public class DarkRoast extends Beverage {
	
	public DarkRoast() {
		description = "DarkRoast Coffee";
	}

	public double cost() {
		return 0.99;
	}

}
