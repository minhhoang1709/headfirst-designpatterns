package headfirst.designpatterns.starbuzz;

public class Espresso extends Beverage {

	public Espresso() {
		description = "Esspresso";
	}

	public double cost() {
		return 1.99;
	}

}
