package headfirst.designpatterns.weatherstation;

public class CurrentConditionDisplay implements DisplayElement, Observer {
	private float tempature;
	private float humidity;
	private Subject weatherData;

	public CurrentConditionDisplay(Subject weatherData) {
		this.weatherData = weatherData;
		weatherData.registerObserver(this);
	}

	public void update(float tempurate, float humidity, float pressure) {
		this.tempature = tempurate;
		this.humidity = humidity;
		display();

	}

	public void display() {
		System.out.println("Current Condition: " + tempature + "F degrees and " + humidity + "% humidity");

	}
}
