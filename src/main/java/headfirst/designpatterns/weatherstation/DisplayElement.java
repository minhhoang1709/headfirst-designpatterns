package headfirst.designpatterns.weatherstation;

public interface DisplayElement {
	void display();
}
