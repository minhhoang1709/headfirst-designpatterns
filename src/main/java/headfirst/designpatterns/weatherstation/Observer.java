package headfirst.designpatterns.weatherstation;

public interface Observer {
	void update(float temp, float humidity, float pressure);
}
