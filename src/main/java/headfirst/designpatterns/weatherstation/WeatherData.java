package headfirst.designpatterns.weatherstation;

import java.util.ArrayList;

public class WeatherData implements Subject {
	private ArrayList<Observer> observers;
	private float tempature;
	private float humidity;
	private float pressure;

	public WeatherData() {
		observers = new ArrayList<Observer>();
	}

	public void registerObserver(Observer o) {
		observers.add(o);

	}

	public void removeObserver(Observer o) {
		int i = observers.indexOf(o);
		if (i >= 0) {
			observers.remove(i);
		}
	}

	public void notifyObservers() {
		for (int i = 0; i < observers.size(); i++) {
			Observer observer = (Observer) observers.get(i);
			observer.update(tempature,humidity , pressure);
		}
	}
	
	public void mesurementsChanged() {
		notifyObservers();
	}
	
	public void setMesurements(float tempature, float humidity, float pressure) {
		this.tempature = tempature;
		this.humidity = humidity;
		this.pressure = pressure;
		mesurementsChanged();
	}
}
