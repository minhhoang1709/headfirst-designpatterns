package headfirst.designpatterns.weatherstation;

public class WeatherStation {
	public static void main(String[] agrs) {
		WeatherData weatherData = new WeatherData();
		
		CurrentConditionDisplay conditionDisplay = new CurrentConditionDisplay(weatherData);
		HeatIndexDisplay heatIndexDisplay = new HeatIndexDisplay(weatherData);
		weatherData.setMesurements(80, 65, 30.4f);
	}
}