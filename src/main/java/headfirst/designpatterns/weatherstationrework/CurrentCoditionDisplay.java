package headfirst.designpatterns.weatherstationrework;

import java.util.Observable;
import java.util.Observer;

import headfirst.designpatterns.weatherstation.DisplayElement;

public class CurrentCoditionDisplay implements Observer, DisplayElement {

	Observable observable;
	private float tempature;
	private float humidity;
	
	public CurrentCoditionDisplay(Observable observable) {
		this.observable = observable;
		observable.addObserver(this);
	}

	public void display() {
		System.out.println("Current conditions: " + tempature + "F degrees and " + humidity + "% humidity");
	}

	public void update(Observable obs, Object arg) {
		if(obs instanceof WeatherData) {
			WeatherData weatherData = (WeatherData)obs;
			this.tempature = weatherData.getTempature();
			this.humidity = weatherData.getHumidity();
			display();
		}

	}

}
