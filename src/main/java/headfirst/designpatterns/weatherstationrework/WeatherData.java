package headfirst.designpatterns.weatherstationrework;

import java.util.Observable;
import java.util.Observer;

public class WeatherData extends Observable {
	private float tempature;
	private float humidity;
	private float pressure;

	public WeatherData() {

	}

	public void measurementsChanged() {
		setChanged();
		notifyObservers();
	}

	public void setMeasurements(float tempature, float humidity, float pressure) {
		this.tempature = tempature;
		this.humidity = humidity;
		this.pressure = pressure;
		measurementsChanged();
	}

	public float getTempature() {
		return tempature;
	}

	public float getHumidity() {
		return humidity;
	}

	public float getPressure() {
		return pressure;
	}

}
