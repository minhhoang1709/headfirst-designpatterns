package headfirst.designpatterns.weatherstationrework;

import java.util.Observable;

public class WeatherStationRework {
	public static void main(String[] agrs) {
		WeatherData weatherData = new WeatherData();
		CurrentCoditionDisplay conditionDisplay = new CurrentCoditionDisplay(weatherData);
		ForecastDisplay forecastDisplay = new ForecastDisplay(weatherData);
		weatherData.setMeasurements(80, 65, 30.4f);
	}
}
